import Vue from "vue";
import VueRouter from "vue-router";
import ProductsPage from "@/views/ProductsPage";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "home",
    component: ProductsPage
  },
  {
    path: "/login",
    name: "login",
    props: true,
    component: () => import("@/components/LoginForm")
  },
  {
    path: "/about",
    name: "about",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue")
  },
  {
    path: "/product/:id",
    name: "product",
    props: true,
    component: () => import("@/views/ProductDetailsPage")
  },
  {
    path: "/cart",
    name: "cart",
    component: () => import("@/views/CartPage")
  },
  {
    path: "/account",
    name: "account",
    component: () => import("@/views/AccountPage"),
    children: [
      {
        path: "",
        name: "account-basic-info",
        alias: "basic-info",
        component: () => import("@/components/AccountInfoForm")
      },
      {
        path: "security",
        name: "security",
        component: () => import("@/components/AccountSecurityForm")
      }
    ]
  },
  {
    path: "*",
    name: "404",
    component: () => import("@/views/404")
  }
];

const router = new VueRouter({
  routes
});

export default router;
