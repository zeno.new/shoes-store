export class CartItem {
  constructor(product, qty) {
    this.product = product;
    this.qty = qty;
  }
}

export default class Cart {
  constructor() {
    this.cartItems = [];
  }

  getTotalPrice() {
    return this.cartItems.reduce((ttl, cartItem) => {
      return ttl + cartItem.product.price * cartItem.qty;
    }, 0);
  }

  getItems() {
    return this.cartItems;
  }

  getItemsCount() {
    return this.cartItems.reduce((count, x) => {
      return count + x.qty;
    }, 0);
  }

  addProduct(product, qty = 1) {
    let addedProduct = this.cartItems.find(item => {
      return product.id == item.product.id;
    });
    if (addedProduct) {
      addedProduct.qty += qty;
    } else {
      let cartItem = new CartItem(product, qty);
      this.cartItems.push(cartItem);
    }
  }
}
