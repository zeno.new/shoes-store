import Cart from "@/models/cart";

export default {
  namespaced: true,
  state: {
    cart: new Cart()
  },
  mutations: {
    addProductToCart(state, product, qty = 1) {
      state.cart.addProduct(product, qty);
    }
  },
  actions: {
    addProductToCart(context, product, qty = 1) {
      context.commit("addProductToCart", product, qty);
    }
  }
};
