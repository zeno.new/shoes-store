export default {
  namespaced: true,
  state: {
    logined: false,
    username: ""
  },
  mutations: {
    login(state, username) {
      state.logined = true;
      state.username = username;
    },
    logout(state) {
      state.logined = false;
      state.username = "";
    }
  },
  actions: {
    login(context, username) {
      context.commit("login", username);
    },
    logout(context) {
      context.commit("logout");
    }
  },
  getters: {
    isLogined: state => {
      return state.logined;
    }
  }
};
